/*

2Rhino - Soluções Tecnológicas e Automação - 2016

Estação Meteorologica

Versão: 1.0

*/
/*
Tabela vento
74  E
376 NE
644 N
725 NW
774 W
514 SW
234 S
150 SE
*/

#include "DHT.h"
#define DHTPIN A1 // pino que estamos conectados
#define DHTTYPE DHT11 // DHT 11
DHT dht(DHTPIN, DHTTYPE);

// Pinos para conexão com Arduino
#define ANEMOMETRO_PIN   2    // Digital 2
#define PLUVIOMETRO_PIN  3    // Digital 3
#define DIR_VENTO_PIN    5    // Analog 5

#include "U8glib.h"
//Utilize a linha abaixo para utilizar a comunicacao serial (Enable, RW, RS, RESET)
U8GLIB_ST7920_128X64_1X u8g(6, 5, 4 ,7);

// CONSTANTES DE CALIBRAÇÃO
// 1 rev/segundo = 1.492 mph = 2.40114125 kph
#define CTE_CAL_ANEMOMETRO 2.4011
// 1 batida = 0.2794 mm
#define CTE_CAL_PLUVIOMETRO 0.2794

// Período entre as medidas em milisegundo
#define PERIODO_ANEMOMETRO  5000
#define PERIODO_DIR_VENTO   5000
#define PERIODO_PLUVIOMETRO  1000

// Vriável que guarda quando foi a ultima interrupcoes
volatile long ultima_batida_pluviometro = 0;

// Variáveis para incrementação
volatile int numRevsAnemometro = 0;
volatile int numBatidasBascula = 0;

// Variáveis para realização do polling
unsigned long proximaMedidaAnemometro = 0;
unsigned long proximaMedidaDirVento = 0;
unsigned long proximaMedidaPluviometro = 0;
unsigned long tempo = 0;

// Direção do vento, valores de leitura para diferenciar cada direção:
// int adc[8] = {26, 45, 77, 118, 161, 196, 220, 256};
int adc[8] = {104, 180, 308, 472, 644, 784, 880, 1024};
// Relação entre os valores analógicos lidos e o que eles representam
// Para facilitar pode-se usar a biblioteca String
char *direcoes[8] = {"W","NW","N","SW","NE","S","SE","E"};
int direcaoInicial = 0;


String input_temp = "";
String input_humid = "";
String input_veloVento = "";
String input_dirVento = "";
String input_volume_chuva = "";


//Funções de callback de interrupção
void contadorAnemometro() {
    numRevsAnemometro++;
}

void contadorPluviometro() {
    if((millis() - ultima_batida_pluviometro) < 10)
      return;
    
    numBatidasBascula++;
    ultima_batida_pluviometro = millis();
    //Serial.println(ultima_batida_pluviometro);
}
  
double calcVelocidadeVento(){
   double velocidadeMedia;

   velocidadeMedia = numRevsAnemometro;
   velocidadeMedia *= 1000.0*CTE_CAL_ANEMOMETRO;
   velocidadeMedia /= PERIODO_ANEMOMETRO;
 
// Resetando contador de pulsos do anemometro
   numRevsAnemometro = 0;
   return velocidadeMedia;
}

double calcQuantidadeChuva(){
  double volumeMedio;
 
  volumeMedio = (numBatidasBascula - 1) * 1.2;
  //volumeMedio *= 1000.0*CTE_CAL_PLUVIOMETRO;
  //volumeMedio /= PERIODO_PLUVIOMETRO;
 
  //numBatidasBascula = 0;
  return volumeMedio;
}

char* calcDirecaoVento() {
  int valor, x;
  valor = analogRead(DIR_VENTO_PIN);
  Serial.print("AnalogRead: ");Serial.println(valor);
  for (x = 0; x < 8; x++) {
    if (adc[x] >= valor)
      break;
  }
 
  // Ajustando direção inicial
  x = (x + direcaoInicial) % 8;
  return direcoes[x];
}

//void u8g_prepare() 
//{  
//  u8g.setFont(u8g_font_6x10);  
//  //u8g.setFontRefHeightExtendedText();  
//  u8g.setDefaultForegroundColor();  
//  u8g.setFontPosTop();  
//}

void plotaTela(String temp, String umid, String veloVento, String volumeChuva) { 

  //TEM QUE MELHORAR ISSO AQUI...
  temp = "Temp: " + temp + "C";
  // Length (with one extra character for the null terminator)
  int str_len_temp = temp.length() + 1; 
  // Prepare the character array (the buffer) 
  char char_temp[str_len_temp];
  temp.toCharArray(char_temp, temp.length() + 1);
  
  umid = "Umid: " + umid + " %";
  // Length (with one extra character for the null terminator)
  int str_len_umid = umid.length() + 1; 
  // Prepare the character array (the buffer) 
  char char_umid[str_len_umid];
  umid.toCharArray(char_umid, umid.length() + 1);

  veloVento = veloVento + " km/h";
  // Length (with one extra character for the null terminator)
  int str_len_veloVento = veloVento.length() + 1; 
  // Prepare the character array (the buffer) 
  char char_veloVento[str_len_veloVento];
  veloVento.toCharArray(char_veloVento, veloVento.length() + 1);

  volumeChuva = "Chuva: " + volumeChuva + " ml";
  // Length (with one extra character for the null terminator)
  int str_len_volumeChuva = volumeChuva.length() + 1; 
  // Prepare the character array (the buffer) 
  char char_volumeChuva[str_len_volumeChuva];
  volumeChuva.toCharArray(char_volumeChuva, volumeChuva.length() + 1);


  //Fonte da Tela
  u8g.setFont(u8g_font_04b_03);  
  
  //Circulo
  u8g.drawCircle(95,31,31);
  
  //Marcacao direcoes
  u8g.drawLine(95,2,95,0);    //Norte Linha
  u8g.drawStr(94,9,"N");      //Norte String
  u8g.drawLine(125,32,124,32);//Leste
  u8g.drawStr(122,35,"L");    //Leste String
  u8g.drawLine(95,60,95,61);  //Sul
  u8g.drawStr(94,59,"S");     //Sul String
  u8g.drawLine(65,32,66,32);  //Oeste
  u8g.drawStr(68,35,"O");     //Oeste String

  //Cursor
  //u8g_SetCursorStyle(,)

  //Ponteiro
  //u8g.drawLine(95, 32, 95, 18);
  
  //Velocidade Vento string 
  u8g.drawStr(78,34, char_veloVento); 
  
  //Linhas de separacao
  u8g.drawLine(62, 0, 62, 64 ); //Linha meio da tela
  u8g.drawLine(0, 17, 62, 17 ); //Primeira Linha  
  u8g.drawLine(0, 28, 62, 28 ); //Segunda Linha
  u8g.drawLine(0, 39, 62, 39 ); //Terceira Linha 
  u8g.drawLine(0, 50, 62, 50 ); //Terceira Linha

  //Vizualizacao dos Dados 
  u8g.drawStr( 12, 6, "13:00 PM");    //Hora
  u8g.drawStr( 3, 15, "31/03/2016"); //Data
  u8g.drawStr( 3, 25, char_temp);   //Temp 
  u8g.drawStr( 3, 37, char_umid);  //umid 
  u8g.drawStr( 3, 47, "Prec.: 20");  //Prec
  u8g.drawStr( 3, 60, char_volumeChuva);  //Pluviometro

}  

        
void setup() {
//  // assign default color value  
//  if ( u8g.getMode() == U8G_MODE_R3G3B2 )   
//   u8g.setColorIndex(255);   // white  
//  else if ( u8g.getMode() == U8G_MODE_GRAY2BIT )  
//   u8g.setColorIndex(1);     // max intensity  
//  else if ( u8g.getMode() == U8G_MODE_BW )  
//   u8g.setColorIndex(1);     // pixel on  

    pinMode(ANEMOMETRO_PIN, INPUT);
    pinMode(PLUVIOMETRO_PIN, INPUT_PULLUP);
    digitalWrite(ANEMOMETRO_PIN, HIGH);
    digitalWrite(PLUVIOMETRO_PIN, HIGH);
    attachInterrupt(0, contadorAnemometro, FALLING);
    attachInterrupt(1, contadorPluviometro, FALLING);
  
    dht.begin(); //Instanciando dht-11
    Serial.begin(9600);

    numBatidasBascula = 0;
}



void loop() {
    //Leitura da Temperatura
    input_temp = round(dht.readTemperature());

    //Leitura da Humidade...
    input_humid = round(dht.readHumidity());

     // Realizando o polling
    tempo = millis();

    double velocidade_vento = 0.0;
    double volume_chuva = 0.0;
    
     if (tempo >= proximaMedidaAnemometro) {
        velocidade_vento = calcVelocidadeVento();
     //   Serial.print("Vento (km/h): ");Serial.println(velocidade_vento, 2);
        input_veloVento = String(velocidade_vento, 2);
        proximaMedidaAnemometro = tempo + PERIODO_ANEMOMETRO;
     }

     if (tempo >= proximaMedidaPluviometro) {
        volume_chuva = calcQuantidadeChuva();
     //   Serial.print("Vento (km/h): ");Serial.println(velocidade_vento, 2);
        input_volume_chuva = String(volume_chuva, 1);
        proximaMedidaPluviometro = tempo + PERIODO_PLUVIOMETRO;
     }
     
//    String aux = "";
//    char aux_char;
//    
//    if (Serial.available()) { // Verificar se há caracteres disponíveis
//        while(Serial.available()){
//          aux_char = Serial.read(); // Armazena caractere lido
//          aux.concat(aux_char);
//        }
//        input_temp = " "+aux;
//    }
//   Serial.println(input_temp);

    if (tempo >= proximaMedidaDirVento) {
      Serial.print("Direcao: ");Serial.println(calcDirecaoVento());
      proximaMedidaDirVento = tempo + PERIODO_DIR_VENTO;
    }

    u8g.firstPage();   
    do 
    {  
      plotaTela(input_temp, input_humid, input_veloVento, input_volume_chuva);
    } 
    while( u8g.nextPage() );
    delay(2000); 
}
